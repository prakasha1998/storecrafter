# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_07_09_052256) do

  create_table "checklist_associations", force: :cascade do |t|
    t.integer "service_id"
    t.integer "service_checklist_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "reopened"
    t.index ["service_checklist_id"], name: "index_checklist_associations_on_service_checklist_id"
    t.index ["service_id"], name: "index_checklist_associations_on_service_id"
  end

  create_table "component_associations", force: :cascade do |t|
    t.integer "service_id"
    t.integer "service_component_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "component_quantity"
    t.index ["service_component_id"], name: "index_component_associations_on_service_component_id"
    t.index ["service_id"], name: "index_component_associations_on_service_id"
  end

  create_table "customers", force: :cascade do |t|
    t.string "customer_name"
    t.integer "customer_phno"
    t.string "customer_location"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "service_checklists", force: :cascade do |t|
    t.string "checklist_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "service_components", force: :cascade do |t|
    t.string "component_name"
    t.integer "component_quantity"
    t.integer "component_cost"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "services", force: :cascade do |t|
    t.string "product_name"
    t.datetime "datetime"
    t.date "requested_delivery"
    t.text "problem_statement"
    t.integer "quantity"
    t.integer "cost"
    t.integer "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "customer_id"
    t.integer "reopened"
  end

end
