class CreateServices < ActiveRecord::Migration[5.2]
  def change
    create_table :services do |t|
      t.string :product_name
      t.datetime :datetime
      t.date :requested_delivery
      t.text :problem_statement
      t.integer :quantity
      t.integer :cost
      t.integer :status , default_insert_value:0
      t.references :Customer, foreign_key: true

      t.timestamps
    end
  end
end
