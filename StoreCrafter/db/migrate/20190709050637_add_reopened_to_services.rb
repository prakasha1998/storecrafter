class AddReopenedToServices < ActiveRecord::Migration[5.2]
  def change
    add_column :services, :reopened, :integer
  end
end
