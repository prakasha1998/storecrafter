class CreateComponentAssociations < ActiveRecord::Migration[5.2]
  def change
    create_table :component_associations do |t|
      t.references :service, foreign_key: true
      t.references :service_component, foreign_key: true

      t.timestamps
    end
  end
end
