class RemoveCustomerIdFromService < ActiveRecord::Migration[5.2]
  def change
    remove_column :services, :Customer_id, :integer
  end
end
