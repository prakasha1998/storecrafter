class AddComponentQuantityToComponentAssociations < ActiveRecord::Migration[5.2]
  def change
    add_column :component_associations, :component_quantity, :integer
  end
end
