class CreateChecklistAssociations < ActiveRecord::Migration[5.2]
  def change
    create_table :checklist_associations do |t|
      t.references :service, foreign_key: true
      t.references :service_checklist, foreign_key: true

      t.timestamps
    end
  end
end
