class CreateCustomers < ActiveRecord::Migration[5.2]
  def change
    create_table :customers do |t|
      t.string :customer_name
      t.integer :customer_phno
      t.string :customer_location

      t.timestamps
    end
  end
end
