class CreateServiceComponents < ActiveRecord::Migration[5.2]
  def change
    create_table :service_components do |t|
      t.string :component_name
      t.integer :component_quantity
      t.integer :component_cost

      t.timestamps
    end
  end
end
