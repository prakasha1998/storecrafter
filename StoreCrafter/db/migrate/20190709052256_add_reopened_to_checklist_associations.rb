class AddReopenedToChecklistAssociations < ActiveRecord::Migration[5.2]
  def change
    add_column :checklist_associations, :reopened, :integer
  end
end
