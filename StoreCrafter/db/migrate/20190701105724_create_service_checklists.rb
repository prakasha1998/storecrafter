class CreateServiceChecklists < ActiveRecord::Migration[5.2]
  def change
    create_table :service_checklists do |t|
      t.string :checklist_name

      t.timestamps
    end
  end
end
