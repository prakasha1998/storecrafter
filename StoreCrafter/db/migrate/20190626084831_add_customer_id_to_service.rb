class AddCustomerIdToService < ActiveRecord::Migration[5.2]
  def change
    add_column :services, :customer_id, :integer
  end
end
