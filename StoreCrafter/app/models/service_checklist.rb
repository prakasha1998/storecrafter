class ServiceChecklist < ApplicationRecord
	has_many :checklist_associations
	has_many :services , through: :checklist_associations

end
