class Service < ApplicationRecord
  belongs_to :customer
  has_many :checklist_associations
  has_many :service_checklists , through: :checklist_associations
  has_many :component_associations
  has_many :service_components , through: :component_associations
  validates :product_name ,presence: true
  validates :datetime ,presence: true
  validates :requested_delivery ,presence: true
  validates :cost ,presence: true
  validates :quantity ,presence: true
  validates :problem_statement ,presence: true

  enum status: { pending:0 , analyzing: 1 , servicing: 2 ,payment_pending: 3 ,delivered: 4 }
  enum reopen: { first_time:0,second_time:1,third_time:2,fourth_time:3}
 end
