class Component < ApplicationRecord
  has_many :services , through: :component_associations
  has_many :component_associations

end
