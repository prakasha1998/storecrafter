class Customer < ApplicationRecord
  has_many :services
  validates :customer_name, presence: true,
            length:{minimum: 5}
  validates :customer_location, presence: true
  validates :customer_phno,presence: true , uniqueness: true
end
