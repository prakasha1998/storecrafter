class ServicesController < ApplicationController
  def index
     @service={}
     @checklist_association = {}
    Service.statuses.keys.each do |status|
      @service[status.to_s] = Service.where(status: status) 
    end
    Service.all.each do |service|
      @checklist_association[service.id] = []
      for i in 0..service.reopened
        
        # @checklist_association[service.id] = { i => ChecklistAssociation.where(service_id: service.id,reopened: i).pluck(:service_checklist_id)}
        @checklist_association[service.id].push(ChecklistAssociation.where(service_id: service.id,reopened: i).pluck(:service_checklist_id))
        # debugger
      end
      
     end
     debugger
    @checklist = ServiceChecklist.pluck(:id , :checklist_name)
    @service
    @checklist_association
  
  end
  def new
    @customer = Customer.find(params[:customer_id])
    @service = @customer.services.new
  end
  def show
    @customer = Customer.find(params[:customer_id])
    # debugger
    @service  = @customer.services.find(params[:id])
  end
  def create
    @customer = Customer.find(params[:customer_id])
    @service = @customer.services.create(service_params)
    @service.update(status:0,reopened:0)
    @checklist = @service.checklist_associations.new
    @checklist.service_checklist_id = 1
    debugger
    @checklist.reopened = 0
    @checklist.save

      redirect_to '/services/index'
  end
  def edit
    @customer = Customer.find(params[:customer_id])
    @service = @customer.services.find(params[:id])
  end
  def update
    # debugger
    @customer = Customer.find(params[:customer_id])
    @service = @customer.services.find(params[:id])

    @service_params = params[:service]
    # debugger
    @servicechecklistids = @service.checklist_associations.all.where(:reopened=>@service.reopened).pluck(:service_checklist_id)
    for i in 6..@service_params.keys.size-1
      # debugger
      if ((@service_params.values[i] == "1")&&((@servicechecklistids.include? @service_params.keys[i].to_i) == false))
        @checklist = @service.checklist_associations.new
        @checklist.service_checklist_id = @service_params.keys[i]
        @checklist.reopened = @service.reopened
        @checklist.save
      elsif ((@service_params.values[i] == "0")&&((@servicechecklistids.include? @service_params.keys[i].to_i) == true))
        # debugger
        @deleteassociations = @service.checklist_associations.all.where(:reopened=>@service.reopened,:service_checklist_id=>@service_params.keys[i]).ids
        ChecklistAssociation.delete(@deleteassociations)
      end
    end
    @service = @customer.services.find(params[:id])
    @updatestatus = @service.checklist_associations.all.where(:reopened=>@service.reopened).pluck(:service_checklist_id)
    debugger
    if ((@updatestatus.size == 2)&&(@updatestatus.last == 2))
      @service.update(status:1)
    elsif(@updatestatus.size == 1)
      @service.update(status:0)
    elsif(@updatestatus.size == 0)
      @service.update(status:0)
    elsif((@updatestatus.size == 3)&&(@updatestatus.last == 3))
      @service.update(status:2)
    elsif((@updatestatus.size == 4)&&(@updatestatus.last == 4))
      @service.update(status:2)
    elsif((@updatestatus.size == 4)&&(@updatestatus.last == 4))
      @service.update(status:2)
    elsif((@updatestatus.size == 5)&&(@updatestatus.last == 5))
      @service.update(status:2)
    elsif((@updatestatus.size == 6)&&(@updatestatus.last == 6))
      @service.update(status:2)
    elsif((@updatestatus.size == 3)&&(@updatestatus.last == 7))
      @service.update(status:3)
    elsif((@updatestatus.size == 7)&&(@updatestatus.last == 7))
      @service.update(status:3)
    elsif((@updatestatus.size == 8)&&(@updatestatus.last == 8))
      @service.update(status:4)
    elsif((@updatestatus.size == 9)&&(@updatestatus.last == 9))
      @service.update(status:4)
    elsif((@updatestatus.size == 9)&&(@updatestatus.include? 9))
      @service.update(status:4)
    end
    # @customer = Customer.find(params[:customer_id])
    if @service.update(service_params)
        redirect_to '/services/index'
      end
  end
  def destroy
    @customer = Customer.find(params[:customer_id])
    @service = @customer.services.find(params[:id])
    @service.checklist_associations.destroy_all
    if @service.destroyed?
      @service.destroy
      redirect_to @customer
    end
  end
  def service_reopened
    @service = Service.find(params[:service_id])
    @service.reopened=@service.reopened+1
    @service.status=0
    @service.checklist_associations.new.service_checklist_id = 1
    if @service.save
        redirect_to '/services/index'
    end
  end
  private
  def service_params
    params.require(:service).permit(:product_name, :datetime, :requested_delivery, :problem_statement, :quantity, :cost, :status, :checklist ,:reopen )
  end
end
