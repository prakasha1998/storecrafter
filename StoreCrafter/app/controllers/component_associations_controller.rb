class ComponentAssociationsController < ApplicationController
  def new
    @component_association = ComponentAssociation.new
    @service_components = ServiceComponent.all
    return @service_components, @component_association
  end
  def create
    # debugger
    @service = Service.find(params[:service_id])
    @parameters = Array.new
    for i in 0..ServiceComponent.all.size-1
      # debugger
      if ((ServiceComponent.ids.include? params[:component_association].keys[i].to_i) && (params[:component_association].values[i]=="1"))
        @parameters.push(params[:component_association].keys[i])
      end
    end
    debugger
    for i in 1..@parameters.size
      @association = @service.component_associations.new(service_component_id: @parameters[i-1],component_quantity: params[:component_association][:component_quantity])
      @association.save
    end
  end
  def index
    @service = Service.find(params[:service_id])
    @component_association = ComponentAssociation.find(params[:id])
    @components = @service.service_components
    return @component_association,@components
  end

  private
  def association_params
    params.require(:component_associations).permit(:service_id ,:component_quantity)
  end
end
