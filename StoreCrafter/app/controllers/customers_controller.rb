class CustomersController < ApplicationController
  def create
    @customer = Customer.new(customer_params)
    if @customer.save
      redirect_to '/customers'
    else
      render 'new'
    end
  end
  def edit
    @customer = Customer.find(params[:id])
  end
  def update
    @customer = Customer.find(params[:id])
    if @customer.update(customer_params)
      redirect_to @customer
    else
      render 'customers/index'
    end
  end
  def destroy
    @customer = Customer.find(params[:id])
    @customer.destroy
    redirect_to @customer
  end
  def show
    @customer = Customer.find(params[:id])
  end
  def new
    @customer = Customer.new
  end
  def index
    @customer = Customer.all
  end
  private
  def customer_params
    params.require(:customer).permit(:customer_name, :customer_phno, :customer_location)
  end
end
