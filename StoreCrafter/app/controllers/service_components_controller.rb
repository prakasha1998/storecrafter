class ServiceComponentsController < ApplicationController
  def index
    @service_component = ServiceComponent.all
  end
  def new
    @service_component = ServiceComponent.new
  end
  def show
    @service_component = ServiceComponent.find(params[:id])
  end
  def create
    debugger
    @service_component = ServiceComponent.new(components_params)
    if @service_component.save
      redirect_to service_components_path
    else
      render 'service_components/new'
    end
  end
  def destroy
    @service_component = ServiceComponent.find(params[:id])
    @service_component.destroy
    redirect_to service_components_path
  end
  def edit
    @service_component = ServiceComponent.find(params[:id])
  end
  def update
    @service_component = ServiceComponent.find(params[:id])
    if @service_component.update(components_params)
      redirect_to service_components_path
  ``else
    render 'service_components/edit'
  ``end
    end
  private
  def components_params
    params.require(:service_component).permit(:component_name, :component_quantity, :component_cost)
  end
end
