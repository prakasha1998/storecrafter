import React from "react"

export class Status_Column extends React.Component{

	render(){
    const status = this.props.status;
    const services = this.props.services;
    const actual_services = this.props.actual_services;
    const checklist = this.props.checklist;
    const checklist_association = this.props.checklist_association
    const rows= [];

    if(Object.keys(services).length == 0 || (services[status].length == 0))
    {
      actual_services[status].forEach((services)=>{
        if(services.status == status)
        {
          rows.push(<Card service = {services}
                                  status = {status} 
                                  checklist ={checklist}
                                  checklist_association={checklist_association[services.id]}/>)
        }
      });
    }
    else{
      services[status].forEach((service)=>{
        if(service.status == status)
        { 
          rows.push(<Card service = {service}
                                  status = {status} 
                                  checklist ={checklist}
                                  checklist_association={checklist_association[service.id]}/>)
        }
      });
    }
    return(<div className="col-xs-2 col-half-offset" id="stylo">
           	<h3 style={{'textAlign':'center'}}>{status}</h3>
            {rows}
          </div>);
    }
}