import Rect from "react"

export class Card extends React.Component{
	render(){
		const service = this.props.service;
    const status = this.props.status;
    const service_id = "#"+service.id;
    const checklist = this.props.checklist;
    const checkboxarray = [];
    const checklist_association = Object.values(this.props.checklist_association);
    var i;
    for(i=service.reopened;i>=0;i--)
    {
        checklist.forEach((checkbox)=>{
          if(checklist_association[i].includes(checkbox[0]))
          {
           checkboxarray.push(<input type="checkbox" checked/>,checkbox[1],<br value={checkbox[0]}/>); 
          }
         else{
          checkboxarray.push(<input type="checkbox" />,checkbox[1]);
          checkboxarray.push(<br value={checkbox[0]}/>);
          }
        });
    }

    return(
    <div>
        <div id="card-main" className="btn btn-default" data-toggle="modal" data-target={service_id}>
          <div>
            <p>{service.product_name}</p>
            <p>{status}</p>
          </div>
        </div>

        <div className="modal fade" id={service.id}  role="dialog" aria-labelledby="myModalLabel">
        	<div className="modal-dialog" role="document">
            	<div className="modal-content">
                	<div className="modal-header">
                		<button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  			<span aria-hidden="true">&times;</span>
                		</button>
                		<h4 className="modal-title" id="myModalLabel">{status}</h4>
              		</div>
              		<div className="modal-body">
		                <h4>Product Name: {service.product_name[0].toUpperCase()+service.product_name.slice(1)}</h4><br></br> 
		                <h4>Date & Time: {service.datetime}</h4><br></br>
		                <h4>Req Delivery: {service.requested_delivery}</h4><br></br>
		                <h4>Problem Statement: {service.problem_statement}</h4><br></br>
		                <h4>quantity: {service.quantity}</h4>
		                <div>
		                    {checkboxarray}
		                </div>
                	</div>
		            <div className="modal-footer">
			            <button type="button" className="btn btn-default" data-dismiss="modal" >Close</button>
			            <button type="button" className="btn btn-primary" >Submit</button>
		            </div>
          		</div>
        	</div>
     	</div>
    </div> 
      );
		}
}