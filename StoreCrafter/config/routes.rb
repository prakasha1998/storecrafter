Rails.application.routes.draw do
  get 'welcome/index'
  resources :customers do
    resources :services do
      collection do
        get 'service_reopened'
        post 'service_reopened'
      end

      resources :component_associations
    end
  end
  resources :service_components
  get 'services/index'
end
